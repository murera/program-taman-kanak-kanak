<?php
	$koneksi=mysqli_connect("localhost","root","","tkk") or die("Gagal Konek");
	$nomor=$_GET['Nomor'];
	$query="select * from raport natural join murid natural join pelajaran where Nomor='$nomor'";
	$data=mysqli_query($koneksi,$query) or die("Gagal Query".$query);
	$sql=mysqli_fetch_array($data);
?>

<html>
<head>
	<title> Formulir 3 </title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<style>
		input[type=text], input[type=password], input[type=date]{
			border: none;
			outline: none;
			width:150%;
			margin:10px 0px 10px 40px;
			border-bottom: 2px solid grey;
		}

		input[type=radio]{
			margin:10px 0px 10px 40px;
		}
		
		select{
			border: none;
			outline: none;
			width:150%;
			margin:10px 0px 10px 40px;
			border-bottom: 2px solid grey;
		}
		
		input[type=button]:focus{
			
		}
		.murid{
			margin-left:50px;
		}
		.card-body{
			margin:30px
		}
	</style>
	</head>

<body>
	<center>
	<br>
	<h1> PENILAIAN PEMBELAJARAN MURID </h1><br>
	<h6> TKK SANTA LUSIA </h6><br>
</center>
	<div class="card-deck" style="margin: 80px; margin-top: 40px; margin-bottom: 0px;">
                <div class="card">
                    <div class="card-body">
<form action="nilai.php" method="post">
		<h5>Penilaian Pelajaran Murid</h5>
		<div class="murid">
		<table>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_anak" readonly
					placeholder="Nama Murid" value="<?php echo $sql['Nama_Murid'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nomor Raport
				</td>
				<td>
					<input type="text" name="no" readonly
					placeholder="Nomor" value="<?php echo $nomor;?>"/>
				</td>
			</tr>
			<tr>
				<td>
					ID Pelajaran
				</td>
				<td>
					<input type="text" name="id_pel" readonly
					placeholder="ID Pelajaran" value="<?php echo $sql['ID_Pel'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nama Pelajaran
				</td>
				<td>
					<input type="text" name="nama_pel" readonly
					placeholder="Nama Pelajaran" value="<?php echo $sql['Nama_Pel'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Kelas
				</td>
				<td>
					<input type="text" name="kelas" readonly
					placeholder="Tempat Lahir" value="<?php echo $sql['Kelas'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Usaha
				</td>
				<td>
					<select name="usaha">
						<option selected disabled>Usaha Murid</option>
						<option value="Perlu Perbaikan">Perlu Perbaikan</option>
						<option value="Baik">Baik</option>
						<option value="Sangat Baik">Sangat Baik</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Pencapaian
				</td>
				<td>
					<select name="pencapaian">
						<option selected disabled>Pencapaian Murid</option>
						<option value="Perlu Bantuan">Perlu Bantuan</option>
						<option value="Dapat Mengikuti">Dapat Mengikuti</option>
						<option value="Mencapai Target">Mencapai Target</option>
						<option value="Melebihi Target">Melebihi Target</option>
					</select>
				</td>
			</tr>
		</table>
		<table>
		<center>
			<tr>
				<td>
					<input type="submit" value="Konfirmasi">
				</td>
			</tr>
		</center>
		</table>
</form>	
</body>
</html>