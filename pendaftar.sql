-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Agu 2020 pada 13.36
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendaftar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kk`
--

CREATE TABLE `kk` (
  `No_KK` char(16) NOT NULL,
  `Status` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kk`
--

INSERT INTO `kk` (`No_KK`, `Status`) VALUES
('2001200220032004', 'Kandung'),
('2005200620072008', 'Angkat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `murid`
--

CREATE TABLE `murid` (
  `NIS` int(7) NOT NULL,
  `NIK` char(16) NOT NULL,
  `Nama_Murid` varchar(50) NOT NULL,
  `Tempat_Lahir` varchar(30) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Jenis_Kelamin` char(1) NOT NULL,
  `Agama` varchar(20) NOT NULL,
  `anak_ke` char(1) DEFAULT NULL,
  `Kelas` char(1) NOT NULL,
  `Periode` year(4) NOT NULL,
  `Password` varchar(8) NOT NULL,
  `No_KK` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `murid`
--

INSERT INTO `murid` (`NIS`, `NIK`, `Nama_Murid`, `Tempat_Lahir`, `Tanggal_Lahir`, `Jenis_Kelamin`, `Agama`, `anak_ke`, `Kelas`, `Periode`, `Password`, `No_KK`) VALUES
(11, '3411181181234567', 'Girisona', 'Bandung', '2013-08-20', 'L', 'Protestan', '1', 'B', 2019, '1234', '2005200620072008');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orang_tua`
--

CREATE TABLE `orang_tua` (
  `No_KTP` char(16) NOT NULL,
  `No_KK` char(16) NOT NULL,
  `Nama_OrangTua` varchar(50) NOT NULL,
  `Tempat_Lahir_Ortu` varchar(20) NOT NULL,
  `Tanggal_Lahir_Ortu` date NOT NULL,
  `Alamat_OrangTua` varchar(30) NOT NULL,
  `Agama_OrangTua` varchar(20) NOT NULL,
  `Pendidikan` varchar(20) NOT NULL,
  `No_Telp` varchar(12) NOT NULL,
  `Pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orang_tua`
--

INSERT INTO `orang_tua` (`No_KTP`, `No_KK`, `Nama_OrangTua`, `Tempat_Lahir_Ortu`, `Tanggal_Lahir_Ortu`, `Alamat_OrangTua`, `Agama_OrangTua`, `Pendidikan`, `No_Telp`, `Pekerjaan`) VALUES
('1001100210031004', '2001200220032004', 'Kevin Kuipper', 'Walanda', '1986-11-22', 'Cimahi', 'Protestan', 'S3', '082345120394', 'CEO'),
('1006100710081009', '2001200220032004', 'Kimberly', 'Irlandia', '1987-08-30', 'Cimahi', 'Protestan', 'S1', '083959182123', 'Sistem Informasi'),
('3001300230033004', '2005200620072008', 'Sahat Butuan', 'Padalarang', '1986-07-09', 'Cimahi', 'Protestan', 'S2', '083959182333', 'PNS'),
('4001400240034004', '2005200620072008', 'Mawar', 'Cimahi', '1988-05-16', 'Cimahi', 'Protestan', 'S1', '083959182222', 'Ibu Rumah Tangga');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kk`
--
ALTER TABLE `kk`
  ADD PRIMARY KEY (`No_KK`);

--
-- Indeks untuk tabel `murid`
--
ALTER TABLE `murid`
  ADD PRIMARY KEY (`NIS`),
  ADD KEY `KTP_Ortu` (`No_KK`);

--
-- Indeks untuk tabel `orang_tua`
--
ALTER TABLE `orang_tua`
  ADD PRIMARY KEY (`No_KTP`),
  ADD KEY `No_KK` (`No_KK`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `murid`
--
ALTER TABLE `murid`
  MODIFY `NIS` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `murid`
--
ALTER TABLE `murid`
  ADD CONSTRAINT `murid_ibfk_1` FOREIGN KEY (`No_KK`) REFERENCES `kk` (`No_KK`);

--
-- Ketidakleluasaan untuk tabel `orang_tua`
--
ALTER TABLE `orang_tua`
  ADD CONSTRAINT `orang_tua_ibfk_1` FOREIGN KEY (`No_KK`) REFERENCES `kk` (`No_KK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
