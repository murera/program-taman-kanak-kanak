<?php
?>

<!doctype html>
<html lang="en">
  <head>
	<title>TKK SANTA LUSIA</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  </head>
<style type="text/css">
		.box{
			background-color: white;
			padding-top: 40px;
			padding-right: 20px;
			border-radius: 5px;
			font-family: trebuchet ms;
			font-size: 15px;
		}
		input{
			outline: none;
			margin-bottom: 20px;
			height: 30px;
			border-radius: 3px;
			border: 1px solid grey;
			padding: 2px 5px 2px 5px;
			width: 100%;
		}
		input[type=submit]{
			width:50%;
			border: none;
			height: 30px;
			font-family: trebuchet ms;
			font-size: 15px;
			background-color: #89AEB2;
			color: white;
			border-radius: 5px;
		}
		input[type=submit]:hover{
			background-color: #84B4C8;
		}
		input[type=date]{
			width:100%;
		}
		input[type=radio]{
			margin-top:0px;
			width: 10%;
			transform: translate(0%,35%);
		}
		select{
			margin-bottom: 20px;
			margin-top: 20px;
			height: 30px;
			width:100%;
			border-radius: 3px;
			padding: 2px 5px 2px 5px;
			font-family: trebuchet ms;
		}
		::-webkit-input-placeholder{
			color: grey;
			font-family: trebuchet ms;
		}
		b{
			color: #c0392b;
		}
	</style>
<body>
        <center>
        <div style="padding:0px 100px 0px 100px;">
            <h1 style="margin: 80px; margin-top: 50px; margin-bottom: 0px;">TKK SANTA LUSIA</h1>
            
	</div>
<div class="card-deck" style="margin: 80px; margin-top: 40px; margin-bottom: 0px;">
                <div class="card">
                    <div class="card-body">
			<h3>Login sebagai Guru</h3>
              		<div class="box">
		<form action="aksi_login.php" method="post">
		<table>
			<tr>
				<td colspan="2">
					<b>NIP</b>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="text" name="nip" placeholder="NIP" required style="width:200px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Password</b>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="password" name="password" placeholder="Password" required/>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<center>
						<input type="submit" name="submit" value="Login"/>
					</center>
				</td>
			</tr>
		</table>
	</form>
	</div>    
                    </div>  
                </div>
                    
                <div class="card">
                    <div class="card-body">
                        <h3>Login sebagai Murid</h3>
              		<div class="box">
		<form action="aksi_murid.php" method="post">
		<table>
			<tr>
				<td colspan="2">
					<b>NIS</b>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="text" name="nis" placeholder="NIS" required style="width:200px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Password</b>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="password" name="password" placeholder="Password" required/>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<center>
						<input type="submit" name="submit" value="Login"/>
					</center>
				</td>
			</tr>
		</table>
	</form>
	</div>
	<p>Daftar sekarang, <a href="daftar.php">disini</a></p>
                    </div>  
                </div>
            </div>
        </div>
</center>
    </body>
</html>