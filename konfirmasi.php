<?php
	$koneksi=mysqli_connect("localhost","root","","pendaftar") or die("Gagal konek database");
	$nis=$_GET['nis'];
	$query="select * from murid where NIS='$nis'";
	$querykk="select * from kk natural join murid where nis='$nis'";
	$queryot="select * from orang_tua natural join kk, murid where NIS='$nis'";
	$data=mysqli_query($koneksi,$query) or die("Gagal Query".$query);
	$datakk=mysqli_query($koneksi,$querykk) or die("Gagal Query".$query);
	$dataot=mysqli_query($koneksi,$queryot) or die("Gagal Query".$query);
	$sql=mysqli_fetch_array($data);
	$sqlkk=mysqli_fetch_array($datakk);
?>

<!DOCTYPE HTML>
<html>
<head>
	<title> Formulir 3 </title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<style>
		input[type=text], input[type=password], input[type=date]{
			border: none;
			outline: none;
			width:150%;
			margin:10px 0px 10px 40px;
			border-bottom: 2px solid grey;
		}

		input[type=radio]{
			margin:10px 0px 10px 40px;
		}
		
		select{
			border: none;
			outline: none;
			width:150%;
			margin:10px 0px 10px 40px;
			border-bottom: 2px solid grey;
		}
		
		input[type=button]:focus{
			
		}
		.murid{
			margin-left:50px;
		}
		.card-body{
			margin:30px
		}
	</style>
	</head>

<body>
	<center>
	<br>
	<h1> PENDAFTARAN CALON MURID </h1><br>
	<h6> TKK SANTA LUSIA </h6><br>
</center>
	<div class="card-deck" style="margin: 80px; margin-top: 40px; margin-bottom: 0px;">
                <div class="card">
                    <div class="card-body">
<form action="aksi_insert.php" method="post">
		<h5>Data Calon Murid</h5>
		<div class="murid">
		<table>
			<tr>
				<td>
					NIS
				</td>
				<td>
					<input type="text" name="nis" placeholder="NIS"/>
				</td>
			</tr>
			<tr>
				<td>
					NIK
				</td>
				<td>
					<input type="text" name="nik" 
					placeholder="NIK" value="<?php echo $sql['NIK'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_anak" readonly
					placeholder="Nama Murid" value="<?php echo $sql['Nama_Murid'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Tempat Lahir
				</td>
				<td>
					<input type="text" name="tempat_lahir_anak" readonly
					placeholder="Tempat Lahir" value="<?php echo $sql['Tempat_Lahir'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir
				</td>
				<td>
					<input type="date" name="tanggal_lahir_anak" readonly
					value="<?php echo $sql['Tanggal_Lahir'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Jenis Kelamin
				</td>
				<td>
					<input type="radio" name="jk" value="L"/> Laki - Laki
					<input type="radio" name="jk" value="P"/> Perempuan
				</td>
			</tr>
			<tr>	
				<td>
					Agama
				</td>
				<td>
					<input type="text" name="agama_anak" 
					placeholder="Agama" value="<?php echo $sql['Agama'];?>"/>
				</td>
			</tr>
			<tr>	
				<td>
					Anak Ke -
				</td>
				<td>
					<input type="text" name="anak"
					value="<?php echo $sql['anak_ke'];?>"/>
				</td>
			</tr>
			<tr>	
				<td>
					Kelas/Kelompok
				</td>
				<td>
					<select name="kelas">
						<option value="<?php echo $sql['Kelas'];?>" selected>TK-<?php echo $sql['Kelas'];?></option>
						<option value="A">TK-A</option>
						<option value="B">TK-B</option>
					</select>
				</td>
			</tr>
		</table>
		</div>
		<br>
		<h5>Data Orang Tua Murid</h5>
		<div class="murid" style="margin-left:50px;">
		<table>
			<tr>
				<td>
					Nomor KK
				</td>
				<td>
					<input type="text" name="no_kk" readonly
					placeholder="Nomor_KK" value="<?php echo $sqlkk['No_KK'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Keterangan
				</td>
				<td>
					<select name="status">
						<option value="<?php echo $sqlkk['Status'];?>" selected>Orang Tua <?php echo $sqlkk['Status'];?></option>
						<option value="Kandung">Orang Tua Kandung</option>
						<option value="Angkat">Orang Tua Angkat</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<h5>Data Ayah</h5>
				</td>
			</tr>
		</table>
		</div>
		<div class="murid" style="margin-left:70px;">
		<?php $sqlot=mysqli_fetch_array($dataot);?>
		<table>
			<tr>
				<td>
					NIK
				</td>
				<td>
					<input type="text" name="nik_ayah" 
					placeholder="NIK" value="<?php echo $sqlot['No_KTP'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_ayah" 
					placeholder="Nama Lengkap" value="<?php echo $sqlot['Nama_OrangTua'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Tempat Lahir
				</td>
				<td>
					<input type="text" name="tempat_lahir_ayah" 
					placeholder="Tempat Lahir" value="<?php echo $sqlot['Tempat_Lahir_Ortu'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir
				</td>
				<td>
					<input type="date" name="tanggal_lahir_ayah"
					value="<?php echo $sqlot['Tanggal_Lahir_Ortu'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Alamat
				</td>
				<td>
					<input type="text" name="alamat_ayah" 
					placeholder="Alamat" value="<?php echo $sqlot['Alamat_OrangTua'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Agama
				</td>
				<td>
					<input type="text" name="agama_ayah" 
					placeholder="Agama" value="<?php echo $sqlot['Agama_OrangTua'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Pendidikan
				</td>
				<td>
					<input type="text" name="pendidikan_ayah" 
					placeholder="Pendidikan Terakhir" value="<?php echo $sqlot['Pendidikan'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nomor Telepon
				</td>
				<td>
					<input type="text" name="telp_ayah" 
					placeholder="Nomor Telepon" value="<?php echo $sqlot['No_Telp'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Pekerjaan
				</td>
				<td>
					<input type="text" name="pekerjaan_ayah" 
					placeholder="Pekerjaan" value="<?php echo $sqlot['Pekerjaan'];?>"/>
				</td>
			</tr>
		</table>
		</div>
		<?php $sqlot=mysqli_fetch_array($dataot);?>
		<div class="murid">
			<tr>
				<td>
					<h5>Data Ibu</h5>
				</td>
			</tr>
		</div>
		<div class="murid" style="margin-left:70px;">
		<table>
			<tr>
				<td>
					NIK
				</td>
				<td>
					<input type="text" name="nik_ibu" 
					placeholder="NIK" value="<?php echo $sqlot['No_KTP'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_ibu" 
					placeholder="Nama Lengkap" value="<?php echo $sqlot['Nama_OrangTua'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Tempat Lahir
				</td>
				<td>
					<input type="text" name="tempat_lahir_ibu" 
					placeholder="Tempat Lahir" value="<?php echo $sqlot['Tempat_Lahir_Ortu'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir
				</td>
				<td>
					<input type="date" name="tanggal_lahir_ibu"
					value="<?php echo $sqlot['Tanggal_Lahir_Ortu'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Alamat
				</td>
				<td>
					<input type="text" name="alamat_ibu" 
					placeholder="Alamat" value="<?php echo $sqlot['Alamat_OrangTua'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Agama
				</td>
				<td>
					<input type="text" name="agama_ibu" 
					placeholder="Agama" value="<?php echo $sqlot['Agama_OrangTua'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Pendidikan
				</td>
				<td>
					<input type="text" name="pendidikan_ibu" 
					placeholder="Pendidikan Terakhir" value="<?php echo $sqlot['Pendidikan'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Nomor Telepon
				</td>
				<td>
					<input type="text" name="telp_ibu" 
					placeholder="Nomor Telepon" value="<?php echo $sqlot['No_Telp'];?>"/>
				</td>
			</tr>
			<tr>
				<td>
					Pekerjaan
				</td>
				<td>
					<input type="text" name="pekerjaan_ibu" 
					placeholder="Pekerjaan" value="<?php echo $sqlot['Pekerjaan'];?>"/>
				</td>
			</tr>
		</table>
		</div>
		<br>
		<div>
		<center>
			<table>
				<tr>
					<td>
						<input type="submit" value="Konfirmasi">
					</td>
				</tr>
			</table>
		</center>
		</div>
	</form>
		    </div>
		</div>
        </div>
	<br>
</body>
</html>