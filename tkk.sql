-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Agu 2020 pada 13.36
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tkk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `NIP` char(16) NOT NULL,
  `Nama_Guru` varchar(50) NOT NULL,
  `Tempat_Lahir` varchar(20) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Alamat` varchar(30) NOT NULL,
  `Tahun_Masuk` year(4) NOT NULL,
  `Password` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`NIP`, `Nama_Guru`, `Tempat_Lahir`, `Tanggal_Lahir`, `Alamat`, `Tahun_Masuk`, `Password`) VALUES
('1501197113031969', 'Alfian', 'Bandung', '1995-12-15', 'Padalarang', 2014, '5678'),
('2402200029082001', 'Reza', 'Bandung', '2000-02-24', 'Rancaekek', 2019, '1234');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kepala_sekolah`
--

CREATE TABLE `kepala_sekolah` (
  `Nomor` int(11) NOT NULL,
  `NIP` char(16) NOT NULL,
  `Periode` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kepala_sekolah`
--

INSERT INTO `kepala_sekolah` (`Nomor`, `NIP`, `Periode`) VALUES
(1, '1501197113031969', 2014),
(2, '2402200029082001', 2019);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kk`
--

CREATE TABLE `kk` (
  `No_KK` char(16) NOT NULL,
  `Status` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kk`
--

INSERT INTO `kk` (`No_KK`, `Status`) VALUES
('2001200220032004', 'Kandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `murid`
--

CREATE TABLE `murid` (
  `NIS` char(7) NOT NULL,
  `NIK` char(16) NOT NULL,
  `Nama_Murid` varchar(50) NOT NULL,
  `Tempat_Lahir` varchar(30) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Jenis_Kelamin` char(1) NOT NULL,
  `Agama` varchar(20) NOT NULL,
  `anak_ke` char(1) DEFAULT NULL,
  `Kelas` char(1) NOT NULL,
  `Periode` year(4) NOT NULL,
  `Password` varchar(8) NOT NULL,
  `No_KK` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `murid`
--

INSERT INTO `murid` (`NIS`, `NIK`, `Nama_Murid`, `Tempat_Lahir`, `Tanggal_Lahir`, `Jenis_Kelamin`, `Agama`, `anak_ke`, `Kelas`, `Periode`, `Password`, `No_KK`) VALUES
('1123456', '5001500250035004', 'Devita', 'Cimahi', '1997-11-14', 'L', 'Budha', '1', 'B', 2019, '1234', '2001200220032004'),
('1234567', '3411181111234567', 'Roy Andreas', 'Bandung', '2013-10-11', 'L', 'Protestan', '1', 'B', 2019, '1234', '2001200220032004'),
('8910112', '3411181111876543', 'Angelina', 'Bandung', '2014-07-22', 'P', 'Protestan', '2', 'A', 2019, '1234', '2001200220032004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orang_tua`
--

CREATE TABLE `orang_tua` (
  `No_KTP` char(16) NOT NULL,
  `No_KK` char(16) NOT NULL,
  `Nama_OrangTua` varchar(50) NOT NULL,
  `Tempat_Lahir` varchar(20) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Alamat_OrangTua` varchar(30) NOT NULL,
  `Agama` varchar(20) NOT NULL,
  `Pendidikan` varchar(20) NOT NULL,
  `No_Telp` varchar(12) NOT NULL,
  `Pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orang_tua`
--

INSERT INTO `orang_tua` (`No_KTP`, `No_KK`, `Nama_OrangTua`, `Tempat_Lahir`, `Tanggal_Lahir`, `Alamat_OrangTua`, `Agama`, `Pendidikan`, `No_Telp`, `Pekerjaan`) VALUES
('1001100210031004', '2001200220032004', 'Kevin Kuipper', 'Walanda', '1986-11-22', 'Cimahi', 'Protestan', 'S3', '082345120394', 'CEO'),
('1006100710081009', '2001200220032004', 'Kimberly', 'Irlandia', '1987-08-30', 'Cimahi', 'Protestan', 'S1', '083959182123', 'Sistem Informasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelajaran`
--

CREATE TABLE `pelajaran` (
  `ID_Pel` char(3) NOT NULL,
  `Nama_Pel` varchar(30) NOT NULL,
  `Kelas` char(1) NOT NULL,
  `NIP` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelajaran`
--

INSERT INTO `pelajaran` (`ID_Pel`, `Nama_Pel`, `Kelas`, `NIP`) VALUES
('P01', 'Bahasa', 'A', '2402200029082001'),
('P02', 'Kognitif', 'A', '1501197113031969'),
('P03', 'Fisik/Motorik', 'B', '1501197113031969'),
('P04', 'Perkembangan Sosial Emosional', 'B', '2402200029082001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `raport`
--

CREATE TABLE `raport` (
  `Nomor` int(11) NOT NULL,
  `NIS` char(16) NOT NULL,
  `ID_Pel` char(3) NOT NULL,
  `Usaha` varchar(30) NOT NULL,
  `Pencapaian` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `raport`
--

INSERT INTO `raport` (`Nomor`, `NIS`, `ID_Pel`, `Usaha`, `Pencapaian`) VALUES
(1, '1234567', 'P03', 'Perlu Perbaikan', 'Perlu Bantuan'),
(2, '1234567', 'P04', 'Baik', 'Mencapai Target'),
(3, '8910112', 'P01', 'Baik', 'Melebihi Target'),
(4, '8910112', 'P02', 'Baik', 'Mencapai Target'),
(5, '1123456', 'P03', '', ''),
(6, '1123456', 'P04', '', '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`NIP`);

--
-- Indeks untuk tabel `kepala_sekolah`
--
ALTER TABLE `kepala_sekolah`
  ADD PRIMARY KEY (`Nomor`),
  ADD KEY `NIP` (`NIP`);

--
-- Indeks untuk tabel `kk`
--
ALTER TABLE `kk`
  ADD PRIMARY KEY (`No_KK`);

--
-- Indeks untuk tabel `murid`
--
ALTER TABLE `murid`
  ADD PRIMARY KEY (`NIS`),
  ADD KEY `KTP_Ortu` (`No_KK`);

--
-- Indeks untuk tabel `orang_tua`
--
ALTER TABLE `orang_tua`
  ADD PRIMARY KEY (`No_KTP`),
  ADD KEY `No_KK` (`No_KK`);

--
-- Indeks untuk tabel `pelajaran`
--
ALTER TABLE `pelajaran`
  ADD PRIMARY KEY (`ID_Pel`),
  ADD KEY `NIP` (`NIP`);

--
-- Indeks untuk tabel `raport`
--
ALTER TABLE `raport`
  ADD PRIMARY KEY (`Nomor`),
  ADD KEY `NIS` (`NIS`),
  ADD KEY `ID_Pel` (`ID_Pel`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `raport`
--
ALTER TABLE `raport`
  MODIFY `Nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kepala_sekolah`
--
ALTER TABLE `kepala_sekolah`
  ADD CONSTRAINT `kepala_sekolah_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `guru` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `murid`
--
ALTER TABLE `murid`
  ADD CONSTRAINT `murid_ibfk_1` FOREIGN KEY (`No_KK`) REFERENCES `kk` (`No_KK`);

--
-- Ketidakleluasaan untuk tabel `orang_tua`
--
ALTER TABLE `orang_tua`
  ADD CONSTRAINT `orang_tua_ibfk_1` FOREIGN KEY (`No_KK`) REFERENCES `kk` (`No_KK`);

--
-- Ketidakleluasaan untuk tabel `pelajaran`
--
ALTER TABLE `pelajaran`
  ADD CONSTRAINT `pelajaran_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `guru` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `raport`
--
ALTER TABLE `raport`
  ADD CONSTRAINT `raport_ibfk_1` FOREIGN KEY (`NIS`) REFERENCES `murid` (`NIS`),
  ADD CONSTRAINT `raport_ibfk_2` FOREIGN KEY (`ID_Pel`) REFERENCES `pelajaran` (`ID_Pel`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
