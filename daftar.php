<!DOCTYPE HTML>
<html>
<head>
	<title> Formulir 3 </title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<style>
		input[type=text], input[type=password], input[type=date]{
			border: none;
			outline: none;
			width:150%;
			margin:10px 0px 10px 40px;
			border-bottom: 2px solid grey;
		}

		input[type=radio]{
			margin:10px 0px 10px 40px;
		}
		
		select{
			border: none;
			outline: none;
			width:150%;
			margin:10px 0px 10px 40px;
			border-bottom: 2px solid grey;
		}
		
		input[type=button]:focus{
			
		}
		.murid{
			margin-left:50px;
		}
		.card-body{
			margin:30px
		}
	</style>
	</head>

<body>
	<center>
	<br>
	<h1> PENDAFTARAN CALON MURID </h1><br>
	<h6> TKK SANTA LUSIA </h6><br>
</center>
	<div class="card-deck" style="margin: 80px; margin-top: 40px; margin-bottom: 0px;">
                <div class="card">
                    <div class="card-body">
<form action="aksi_daftar.php" method="post">
		<h5>Data Calon Murid</h5>
		<div class="murid">
		<table>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_anak" placeholder="Nama Lengkap"/>
				</td>
			</tr>
			<tr>
				<td>
					NIK
				</td>
				<td>
					<input type="text" name="nik" placeholder="NIK"/>
				</td>
			</tr>
			<tr>
				<td>
					Tempat Lahir
				</td>
				<td>
					<input type="text" name="tempat_lahir_anak" placeholder="Tempat Lahir"/>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir
				</td>
				<td>
					<input type="date" name="tanggal_lahir_anak"/>
				</td>
			</tr>
			<tr>
				<td>
					Jenis Kelamin
				</td>
				<td>
					<input type="radio" name="jk" value="L"/> Laki - Laki
					<input type="radio" name="jk" value="P"/> Perempuan
				</td>
			</tr>
			<tr>	
				<td>
					Agama
				</td>
				<td>
					<input type="text" name="agama_anak" placeholder="Agama"/>
				</td>
			</tr>
			<tr>	
				<td>
					Anak Ke -
				</td>
				<td>
					<input type="text" name="anak"/>
				</td>
			</tr>
			<tr>	
				<td>
					Kelas/Kelompok
				</td>
				<td>
					<select name="kelas">
						<option selected disabled>Pilihan TK</option>
						<option value="A">TK-A</option>
						<option value="B">TK-B</option>
					</select>
				</td>
			</tr>
		</table>
		</div>
		<br>
		<h5>Data Orang Tua Murid</h5>
		<div class="murid" style="margin-left:50px;">
		<table>
			<tr>
				<td>
					Nomor KK
				</td>
				<td>
					<input type="text" name="no_kk" placeholder="Nomor_KK"/>
				</td>
			</tr>
			<tr>
				<td>
					Keterangan
				</td>
				<td>
					<select name="status">
						<option selected disabled>Keterangan</option>
						<option value="Kandung">Orang Tua Kandung</option>
						<option value="Angkat">Orang Tua Angkat</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<h5>Data Ayah</h5>
				</td>
			</tr>
		</table>
		</div>
		<div class="murid" style="margin-left:70px;">
		<table>
			<tr>
				<td>
					NIK
				</td>
				<td>
					<input type="text" name="nik_ayah" placeholder="NIK"/>
				</td>
			</tr>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_ayah" placeholder="Nama Lengkap"/>
				</td>
			</tr>
			<tr>
				<td>
					Tempat Lahir
				</td>
				<td>
					<input type="text" name="tempat_lahir_ayah" placeholder="Tempat Lahir"/>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir
				</td>
				<td>
					<input type="date" name="tanggal_lahir_ayah"/>
				</td>
			</tr>
			<tr>
				<td>
					Alamat
				</td>
				<td>
					<input type="text" name="alamat_ayah" placeholder="Alamat"/>
				</td>
			</tr>
			<tr>
				<td>
					Agama
				</td>
				<td>
					<input type="text" name="agama_ayah" placeholder="Agama"/>
				</td>
			</tr>
			<tr>
				<td>
					Pendidikan
				</td>
				<td>
					<input type="text" name="pendidikan_ayah" placeholder="Pendidikan Terakhir"/>
				</td>
			</tr>
			<tr>
				<td>
					Nomor Telepon
				</td>
				<td>
					<input type="text" name="telp_ayah" placeholder="Nomor Telepon"/>
				</td>
			</tr>
			<tr>
				<td>
					Pekerjaan
				</td>
				<td>
					<input type="text" name="pekerjaan_ayah" placeholder="Pekerjaan"/>
				</td>
			</tr>
		</table>
		</div>
		<div class="murid">
			<tr>
				<td>
					<h5>Data Ibu</h5>
				</td>
			</tr>
		</div>
		<div class="murid" style="margin-left:70px;">
		<table>
			<tr>
				<td>
					NIK
				</td>
				<td>
					<input type="text" name="nik_ibu" placeholder="NIK"/>
				</td>
			</tr>
			<tr>
				<td>
					Nama Lengkap
				</td>
				<td>
					<input type="text" name="nama_ibu" placeholder="Nama Lengkap"/>
				</td>
			</tr>
			<tr>
				<td>
					Tempat Lahir
				</td>
				<td>
					<input type="text" name="tempat_lahir_ibu" placeholder="Tempat Lahir"/>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir
				</td>
				<td>
					<input type="date" name="tanggal_lahir_ibu"/>
				</td>
			</tr>
			<tr>
				<td>
					Alamat
				</td>
				<td>
					<input type="text" name="alamat_ibu" placeholder="Alamat"/>
				</td>
			</tr>
			<tr>
				<td>
					Agama
				</td>
				<td>
					<input type="text" name="agama_ibu"  placeholder="Agama"/>
				</td>
			</tr>
			<tr>
				<td>
					Pendidikan
				</td>
				<td>
					<input type="text" name="pendidikan_ibu"  placeholder="Pendidikan Terakhir"/>
				</td>
			</tr>
			<tr>
				<td>
					Nomor Telepon
				</td>
				<td>
					<input type="text" name="telp_ibu" placeholder="Nomor Telepon"/>
				</td>
			</tr>
			<tr>
				<td>
					Pekerjaan
				</td>
				<td>
					<input type="text" name="pekerjaan_ibu"  placeholder="Pekerjaan"/>
				</td>
			</tr>
		</table>
		</div>
		<br>
		<div>
		<center>
			<table>
				<tr>
					<td>
						<input type="submit" value="Daftar">
					</td>
				</tr>
			</table>
		</center>
		</div>
	</form>
		    </div>
		</div>
        </div>
	<br>
</body>
</html>